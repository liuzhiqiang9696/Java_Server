package com.liuzhi.userController;
import com.liuzhi.UserService.UserService;
import com.liuzhi.UserService.UserServiceImpl;
import com.liuzhi.game.UserEntity;
/**
 * 处理客户端的数据
 * @author LIUZHI
 *
 */
public class UserController {
	private UserService userService ;
	public UserController() {
		userService = new UserServiceImpl();
	}
	@SuppressWarnings("unused")
	public String login(String data) {
		/**
		 * 获取数据的子字符串,用户名
		 */
		String user_name = data.substring(0,data.indexOf(","));
		//获取数据的子字符串,密码
		String password = data.substring(data.indexOf(",")+1,data.length());
		//若返回的数据为1，则对应为登录成功，若返回的数据为0，则对应为登录失败
		if(null == user_name || null == password) {
			return "请输入账号或密码";
		}
		UserEntity userEntity = userService.verifyLogin(user_name, password);
		if(null != userEntity) {
			String str = userEntity.getUser_name();
			return "登录成功"+str+","+userEntity.getMoney_quantity()+"."+userEntity.getCoin_quantity();
		}
		else {
			return "请检查账号或密码";
		}
	}
	public String register(UserEntity userEntity) {
		//获取实体对象用户名
		String user_name= userEntity.getUser_name();
		//获取实体对象得密码
		String pass_word=userEntity.getPass_word();
		if(null == user_name || null == pass_word) {
			return "请检查账号或密码";
		}
		//如果用户名或密码不为空，那么就执行注册操作
		int result=userService.userRegister(user_name, pass_word, userEntity.getGender(), userEntity.getAge());
		if(result == 0) {
			return "用户已存在";
		}
		else if(result == 1) {
			return "注册成功";
		}
		else if(result == 2) {
			return "账号大于等于6位账号并且不能含有中文字符";
		}
		else{
			return "密码不合法";
		}
	}
	public String userMoneyUpdate(String data) {
		String money_Quantity1 = data.substring(0,data.indexOf(","));
		int money_Quantity2=Integer.valueOf(money_Quantity1);
		String user_name=data.substring(data.indexOf(",")+1);
		int result=userService.userUpdateMoney_Quantity(money_Quantity2, user_name);
		UserEntity userEntity =UserServiceImpl.userDao.findByUsername(user_name);
		if(result==1) {
			return "充值成功"+userEntity.getMoney_quantity();
			
		}
		return "充值失败";
	}
}
