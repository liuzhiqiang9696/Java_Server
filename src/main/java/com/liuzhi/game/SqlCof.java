package com.liuzhi.game;
import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


public class SqlCof {
/**
 * 打开sqlSession会话
 * @return
 */
	@SuppressWarnings("null")
	public static SqlSession getSqlSession () {
		InputStream cof;
		SqlSession sqlSession = null;
		try {
			cof = Resources.getResourceAsStream("mybatis-config.xml");
			//创建会话工厂
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(cof);
			sqlSession = sqlSessionFactory.openSession(true);
			System.out.println("连接数据库成功！");
		} catch (IOException e) {
			sqlSession.close();
		}
		return sqlSession;
	}
}
