package com.liuzhi.game;
/**
 * user实体类，对应数据库表的6个字段
 * 1.用户名
 * 2.密码
 * 3.年龄性别
 * 4.年龄
 * 5.点券
 * 6.金币
 * @author LIUZH
 *
 */
public class UserEntity {
	
	private String user_name;
	private String pass_word;
	private String gender;
	private int age;
	private int money_quantity;
	private int coin_quantity;
	
	
	public UserEntity(String user_name, String pass_word, String gender, int age, int money_quantity,
			int coin_quantity) {
		super();
		this.user_name = user_name;
		this.pass_word = pass_word;
		this.gender = gender;
		this.age = age;
		this.money_quantity = money_quantity;
		this.coin_quantity = coin_quantity;
	}
	public int getMoney_quantity() {
		return money_quantity;
	}
	public void setMoney_quantity(int money_quantity) {
		this.money_quantity = money_quantity;
	}
	public int getCoin_quantity() {
		return coin_quantity;
	}
	public void setCoin_quantity(int coin_quantity) {
		this.coin_quantity = coin_quantity;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPass_word() {
		return pass_word;
	}
	public void setPass_word(String pass_word) {
		this.pass_word = pass_word;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "UserEntity [user_name=" + user_name + ", pass_word=" + pass_word + ", gender=" + gender + ", age=" + age
				+ ", money_quantity=" + money_quantity + ", coin_quantity=" + coin_quantity + "]";
	}
	
}
