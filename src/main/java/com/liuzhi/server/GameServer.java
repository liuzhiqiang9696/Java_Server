package com.liuzhi.server;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.liuzhi.ParsingData.DataManager;
import com.liuzhi.game.UserEntity;
import com.liuzhi.userController.UserController;
/**
 * 1.GameServer实现了Runnable接口
 * 2.GameServer可以监听多个客户端的请求，并建立响应的Socket
 * @author LIUZHI
 *
 */
public class GameServer implements Runnable{
	public GameServer() {}
	public static UserController userController = new UserController();
	public void run() {
		//List<Login> loginAndRegisters = new ArrayList<Login>();
				try {
					//创建指定服务器，给定指定端口
					ServerSocket serverSocket = new ServerSocket(8888);
					
					System.out.println("服务器启动成功...");
					while (true) {
						//服务器开始监听，被阻塞
						Socket socket = serverSocket.accept();
						String IPAddress = socket.getInetAddress().getHostAddress();
						if(IPAddress!=null) {
							System.out.println(IPAddress + "-设备已连接"); 
						}
						new DataManager(socket);
						
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("服务器启动失败！");
				}
	}
}
