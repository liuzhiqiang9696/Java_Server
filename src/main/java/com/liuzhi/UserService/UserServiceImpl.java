package com.liuzhi.UserService;
import java.util.regex.Pattern;
import com.liuzhi.UserDao.UserDao;
import com.liuzhi.game.SqlCof;
import com.liuzhi.game.UserEntity;
public class UserServiceImpl implements UserService{
	public static UserDao userDao = SqlCof.getSqlSession().getMapper(UserDao.class);
	public UserEntity verifyLogin(String user_name, String password) {
		UserEntity userEntity = null;
		if(userDao !=null) {
		userEntity = userDao.findByUsername(user_name);
			
		}
		if(null == userEntity){
			return null;
		}
		if(password.equals(userEntity.getPass_word())) {
			return userEntity;
		}
		return null;
	}

	public int userRegister(String user_name, String password, String gender, int age) {
		if(userDao.findByUsername(user_name) != null) {
			return 0;
		}
		//正则表达式,规定用户名为6-18、,位长度，包含数字和字母
		String user_nameLimited = "^[0-9A-Za-z]{6,16}$";
		//正则表达式,限制密码长度,允许密码的类型
		String passwordLimited = "^([A-Z]|[a-z]|[0-9]|[-=;,./~!@#$%^*()_+}{:?]|[|]){8,16}$";
		//用户名和用户名正则表达式规则不匹配
		if(!Pattern.matches(user_nameLimited, user_name)) {
			return 2;
		}
		//密码和密码正则表达式规则不匹配
		if(!Pattern.matches(passwordLimited, password)) {
			return 3;
		}
		//执行插入操作，插入成功并返回1
		UserEntity userEntity = new UserEntity(user_name, password, gender, age,0,0);
		return userDao.userRegister(userEntity);
	}
	
		public int userUpdateMoney_Quantity(int money_quantity, String user_name) {
			UserEntity userEntity = null;
			userEntity=userDao.findByUsername(user_name);
			if(null != userEntity) {
				int originQuantity=userEntity.getMoney_quantity();
				return userDao.updateMoney_Quantity(money_quantity+originQuantity, user_name);
			}else {
				return 0;
			}
			
		}

    
}
