package com.liuzhi.UserService;

import com.liuzhi.game.UserEntity;

/**
 * UserService包含两个方法。
 * 1.验证用户登录
 * 2.用户注册
 * @author LIUZH
 *
 */
public interface UserService {
	
	public UserEntity verifyLogin(String user_name, String password);
	public int userRegister(String user_name, String password,String gender, int age);
	public int userUpdateMoney_Quantity(int money_quantity,String user_name);

}
