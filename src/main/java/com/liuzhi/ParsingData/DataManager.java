package com.liuzhi.ParsingData;
/**
 * 该类用于解析数据，并且根据controller返回的值再向客户端响应请求，发送数据
 */
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import com.alibaba.fastjson.JSONObject;
import com.liuzhi.game.UserEntity;
import com.liuzhi.server.GameServer;
import com.liuzhi.userController.UserController;
public class DataManager  {
	//定义一个读取字节流对象
	private BufferedInputStream dis=null;
	//定义一个输出字节流对象
	private DataOutputStream dos = null;
	//用于存放登录的消息
	public  String loginMessage = null;;
	//用于存放注册的消息
	public  String registerMessage=null;
	//用于存放充值消息
	public String chargeMessage=null;
	public String getChargeMessage() {
		return chargeMessage;
	}

	public void setChargeMessage(String chargeMessage) {
		this.chargeMessage = chargeMessage;
	}
	//用于存放客户端发送过来的消息，等待解析
	private String message;
	public DataManager(Socket socket) {
			try {
				//获取客户端发送过来的字节流对象
				this.dis = new BufferedInputStream(socket.getInputStream());
				//获取将要从服务器发送出去的字节流对象
				this.dos = new DataOutputStream(socket.getOutputStream());
				//定义一个字节数组，用于存放数据
				byte[] bt = new byte[1024];
				while(true) {
					
					//从字节流对象中读取数据并将其放在bt字节数组中，返回的是字节的长度
					int length = dis.read(bt);
					
					//如果开始读取并且还没有到达数据的末尾
					if (length != -1) {
						//把字节数组当中的数据转行为字符串
						this.message = new String(bt, 0, length);
						System.out.println("**************************");
						System.out.println(this.message);
					} else {
						break;
					}
					//如果从客户端接受到的消息不是空对象并且不为空字符串
					if (null != message && !message.equals("")) {
						//如果消息前5个字符为login，就把login去掉，并把剩余的字符串赋值给登录的消息
						if (message.substring(0, 5).equals("login")) {
							this.loginMessage = message.substring(5);
							System.out.println("loginMessage:" + loginMessage);
							System.out.println("**************************");
						}
						//如果消息前8个字符为register，就把register去掉，并把剩余的字符串赋值给注册的消息
						else if (message.substring(0, 8).equals("register")) {
							this.registerMessage = message.substring(8);
							System.out.println("registerMessage:-----" + registerMessage);
						}else if(message.substring(0, 6).equals("charge")) {
							this.chargeMessage=message.substring(6);
						}
					}
					//定义一个Method对象
					Method m;
					String msg = this.getMessage();
					if(msg != null && "login".equals(msg.substring(0,5))) {
						String loginMsg = null;
						try {
							/**
							 * JAVA反射机制是在运行状态中，对于任意一个类，
							 * 都能够知道这个类的所有属性和方法;对于任意一个对象，
							 * 都能够调用它的任意一个方法;
							 * 这种动态获取的信息以及动态调用对象的方法的功能称为java语言的反射机制。
							 */
							m = UserController.class.getMethod("login", String.class);
							//调用这个userController下面的m方法，并传递getLoginMessage()参数
							loginMsg = (String) m.invoke(GameServer.userController, getLoginMessage());
						} catch (NoSuchMethodException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (SecurityException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IllegalAccessException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IllegalArgumentException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InvocationTargetException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
						if(loginMsg.substring(0,4).equals("登录成功")) {
							dos.write(loginMsg.getBytes("utf-8"));
							System.out.println("登录成功");
							dos.flush();
						}if(loginMsg.equals("请检查账号或密码")){
							dos.write("2".getBytes("utf-8"));
							System.out.println("发送数据：" + 2);
							System.out.println("登录失败！！");
							dos.flush();
						}
					
					}
					 /**
				     * 如果消息头为register，那么就先进行查询，再验证用户的账号和密码是否匹配
				     */
					if(msg !=null && "register".equals(msg.substring(0,8))) {
						try {
							/**
							 * JAVA反射机制是在运行状态中，对于任意一个类，
							 * 都能够知道这个类的所有属性和方法;对于任意一个对象，
							 * 都能够调用它的任意一个方法;
							 * 这种动态获取的信息以及动态调用对象的方法的功能称为java语言的反射机制。
							 */
							m = UserController.class.getMethod("register", UserEntity.class);
							String registerMsg = null;
							if(null != getUserEntity()) {
								UserEntity userEntity = getUserEntity();
								//调用这个userController下面的m方法，并传递getLoginMessage()参数，最后强转为字符串
		 						 registerMsg =(String) m.invoke(GameServer.userController, userEntity);
							}
							//验证返回注册信息
							if(registerMsg.equals("用户已存在")) {
								//向客户端发送数据
								dos.write("3".getBytes("utf-8"));
								dos.flush();
							}else if(registerMsg.equals("账号大于等于6位账号并且不能含有中文字符")) {
								//向客户端发送数据
								dos.write("4".getBytes("utf-8"));
								System.out.println("账号大于等于6位账号并且不能含有中文字符");
								dos.flush();
							}else if(registerMsg.equals("密码不合法")) {
								//向客户端发送数据
								dos.write("5".getBytes("utf-8"));
								System.out.println("密码不合法");
								dos.flush();
							}else if(registerMsg.equals("请检查账号或密码")) {
								return;
							}else{
								//向客户端发送数据
								dos.write("6".getBytes("utf-8"));//注册成功
								System.out.println("注册成功");
								dos.flush();
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
		/**
		 * 处理充值消息
		 */ 
					if(msg != null && msg.substring(0,6).equals("charge")) {
						try {
							m=UserController.class.getMethod("userMoneyUpdate", String.class);
							try {
								String chargeMsg=(String) m.invoke(GameServer.userController, getChargeMessage());
								if(chargeMsg.substring(0,4).equals("充值成功")) {
									dos.write(chargeMsg.getBytes("utf-8"));
									System.out.println("充值成功");
									System.out.println(msg.substring(6));
									dos.flush();
								}else {
									dos.write("7".getBytes("utf-8"));
									System.out.println("充值失败");
									dos.flush();
								}
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (NoSuchMethodException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
							
				}
			} catch (IOException e) {
				try {
					dis.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
	
			
	}
	
	public  UserEntity getUserEntity() {
		//将客户端发来的注册请求字符串包装成JsonObject
		JSONObject jsonObject = JSONObject.parseObject(registerMessage);
		UserEntity userEntity = null;
		try {
			//将JsonObject强行转换为userEntity对象
			userEntity = (UserEntity)jsonObject.toJavaObject(UserEntity.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("转换失败！");
		}
		return userEntity;
	}
	
	public String getLoginMessage() {
		return loginMessage;
	}
	public void setLoginMessage(String loginMessage) {
		this.loginMessage = loginMessage;
	}
	public String getRegisterMessage() {
		return registerMessage;
	}
	public void setRegisterMessage(String registerMessage) {
		this.registerMessage = registerMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}


