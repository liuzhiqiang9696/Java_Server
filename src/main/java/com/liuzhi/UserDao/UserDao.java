package com.liuzhi.UserDao;

import org.apache.ibatis.annotations.Param;

import com.liuzhi.game.UserEntity;

public interface UserDao {
	
	//通过用户名查询用户是否存在
	public UserEntity findByUsername(String User_name);
	//注册用户信息(添加用户)
	public int userRegister(UserEntity userEntity);
	//更新用户的点券信息
	public int updateMoney_Quantity(@Param("money_quantity")int money_quantity,@Param("user_name")String user_name);
//	//更新用户的金币信息
//	public int updateCoin_Quantity(UserEntity userEntity);
}
