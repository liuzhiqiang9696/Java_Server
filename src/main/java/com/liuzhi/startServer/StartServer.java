package com.liuzhi.startServer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.liuzhi.server.GameServer;
public class StartServer {
	public static void main(String[] args) {
		//创建一个线程池
		ExecutorService es = Executors.newCachedThreadPool();
		es.execute(new GameServer());
	}
}
